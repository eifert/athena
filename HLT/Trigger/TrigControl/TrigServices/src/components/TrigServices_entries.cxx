#include "TrigServices/TrigMessageSvc.h"
#include "../TrigMonTHistSvc.h"
#include "TrigServices/HltEventLoopMgr.h"
#include "TrigServices/HltROBDataProviderSvc.h"
#include "../TrigCOOLUpdateHelper.h"

DECLARE_COMPONENT( TrigMessageSvc )
DECLARE_COMPONENT( TrigMonTHistSvc )
DECLARE_COMPONENT( HltEventLoopMgr )
DECLARE_COMPONENT( HltROBDataProviderSvc )
DECLARE_COMPONENT( TrigCOOLUpdateHelper )
DECLARE_COMPONENT( THistSvcHLT )
